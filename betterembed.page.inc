<?php

/**
 * @file
 * Contains betterembed.page.inc.
 *
 * Page callback for BetterEmbed Entity entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for BetterEmbed Entity templates.
 *
 * Default template: betterembed.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_betterembed(array &$variables) {
  // Fetch BetterEmbed Entity.
  $betterembed = $variables['elements']['#betterembed'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
