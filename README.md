<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Useful Links
 * Documentation
 * Maintainers

INTRODUCTION
------------

Add embeds from Twitter, Facebook, Youtube to your website WITHOUT iframes or
third party libraries. You can include Teasers from ANY Website, too!
The best way to enrich your content - Free yourself from third parties.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/betterembed

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/betterembed?status=All&categories=All

REQUIREMENTS
------------

This module requires the following module:

 * https://www.drupal.org/project/media


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  When enabled, you get an additional field formatter option called "BetterEmbed" for
  the field type link. A new Media Type BetterEmbed is created which will store all
  BetterEmbed fields from the API. Configuration of the Module is available via
  /admin/config/betterembed

  Add your link field the Formatter BetterEmbed to enrich the links.

  It comes with two pre-defined Views (Default and Cover), so you can use them as a base
  for your customizations, or just create a new Media View and add a template to
  your theme.

USEFUL LINKS
------------

Details, Demo and more infos about the project and supported Systems
https://www.acolono.com/betterembed

Try the API
https://api.betterembed.com/swagger/index.html

Communicate
twitter: https://twitter.com/betterembed


DOCUMENTATION
-------------

https://app.gitbook.com/@acolono/s/betterembed/


MAINTAINERS
-----------

Current maintainers:
 * Alexander Sulz (vierlex) - https://www.drupal.org/u/vierlex
 * Dejan Lacmanovic (dejan0) - https://www.drupal.org/u/dejan0
 * Nico Grienauer (Grienauer) - https://www.drupal.org/u/grienauer


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
