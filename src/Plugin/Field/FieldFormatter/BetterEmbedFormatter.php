<?php

/**
 * @file
 * Contains \Drupal\url_embed\Plugin\Field\FieldFormatter\LinkEmbedFormatter.
 */

namespace Drupal\betterembed\Plugin\Field\FieldFormatter;

use Drupal\betterembed\BetterEmbedManager;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\url_embed\UrlEmbedHelperTrait;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'url_embed' formatter.
 *
 * @FieldFormatter(
 *   id = "betterembed",
 *   label = @Translation("BetterEmbed"),
 *   field_types = {
 *     "link",
 *     "betterembed"
 *   }
 * )
 */
class BetterEmbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'view_mode' => 'default',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $view_modes = \Drupal::service('entity_display.repository')
      ->getViewModes('media');
    $options = [];
    $options['default'] = 'Default';

    foreach ($view_modes as $id => $view_mode) {
      $options[$id] = $view_mode['label'];
    }

    return [
        'view_mode' => [
          '#type' => 'select',
          '#title' => t('View mode of the BetterEmbed.'),
          '#options' => $options,
          '#default_value' => $this->getSetting('view_mode'),
        ],
      ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Betterembed view mode: @view_mode', ['@view_mode' => $this->getSetting('view_mode')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = array();

    foreach ($items as $delta => $item) {
      if ($url = $item->getUrl()->toString()) {
        try {
          $entity = \Drupal::service('betterembed')
            ->loadByURL(Html::decodeEntities($url));

          $view_mode = $this->getSetting('view_mode');
          $view_builder = \Drupal::entityTypeManager()->getViewBuilder('media');
          $build = $view_builder->view($entity, $view_mode);
          $element[$delta] = $build;
        }
        catch (\Exception $exception) {
          watchdog_exception('betterembed', $exception);
        }
      }
    }

    return $element;
  }

}
