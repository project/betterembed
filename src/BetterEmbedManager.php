<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14.12.18
 * Time: 16:19
 */

namespace Drupal\betterembed;

use Drupal\Component\Utility\Random;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaInterface;
use GuzzleHttp\Client;
use Psr\Container\ContainerInterface;

class BetterEmbedManager {

  /**
   * @var Config
   */
  protected $config;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $manager;

  /**
   * Constructs a new GlobalConfigForm object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $manager) {
    $this->config = $config_factory->get('betterembed.globalconfig');
    $this->manager = $manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * @{inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * @{inheritdoc}
   */
  public function setConfig(Config $config) {
    $this->config = $config;
  }

  /**
   * Load by URL and create by default
   */
  public function loadByURL($url, $allow_create_new_entity = TRUE) {
    $storage = $this->manager->getStorage('media');
    $entity = $storage->loadByProperties([
      'name' => $url,
      'bundle' => 'betterembed',
    ]);

    if (empty($entity) && $allow_create_new_entity) {
      $entity = $this->fetchEmbed($url);
      $entity->save();
      return $entity;
    }

    return reset($entity);
  }

  /**
   * @{inheritdoc}
   */
  public function fetchEmbed($url) {
    $server_url = $this->config->get('betterembed.server_url') ?? "https://api.betterembed.com";
    $client = new Client(['base_uri' => $server_url]);
    $request = $client->request("GET", '/api/v0/item', ['query' => ['url' => $url]]);
    $status = $request->getStatusCode();
    $error = NULL;

    // TODO: do some try / catch
    switch ($status) {
      case 200:
        $json = json_decode($request->getBody()->getContents());
        $entity = $this->createFromJson($url, $json);
        if ($entity) {
          $this->fetchThumbnail($entity);
        }
        else {
          $error = 'Creation of Media entity failed';
        }
        break;
      case 400:
        $json = json_decode($request->getBody()->getContents());
        $error = $json['detail'];
        break;
      case 429:
        $error = 'Rate Limit Reached';
        break;

      default:
        $error = json_decode($request->getBody()->getContents());
        break;
    }

    if (!empty($error)) {
      \Drupal::logger('BetterEmbed')
        ->error("Couldn't create Embed from @url: @error", [
          '@url' => $url,
          '@error' => $error,
        ]);
    }


    return $entity;
  }

  /**
   * Fetch again BetterEmbed item (refresh).
   */
  public function refreshBetterEmbed($entity_id) {
    $entity = $this->manager->getStorage('media')->load($entity_id);
    $url = $entity->field_url->uri;
    $fetched_entity = $this->fetchEmbed($url);

    $fields = $fetched_entity->getFields();

    foreach ($fields as $field_name => $field) {
      if (mb_strpos($field_name, 'field_') !== FALSE ||
        $field_name == 'name' ||
        $field_name == 'thumbnail') {

        // remove old File entity referenced by $entity
        // which will reference a new File entity which was created by fetchEmbed()
        if ($field_name == 'thumbnail') {
          $old_file = $entity->get($field_name)->entity;
          if (!empty($old_file)) {
            $old_file->delete();
          }
        }

        $new_value = $fetched_entity->get($field_name)->getValue();
        $entity->get($field_name)->setValue($new_value);
      }
    }

    $entity->save();
    return $entity;
  }

  protected function createFromJson($name, $json) {
    $values = [];
    if (!is_array($json)) {
      // make sure Name value length is not exceeding 255 characters
      $name = Unicode::truncate($name, 255);
      $values = [
        'bundle' => 'betterembed',
        'name' => !empty($name) ? $name : '',
        'field_embed_html' => !empty($json->embedHtml) ? $json->embedHtml : '',
        'field_url' => !empty($json->url) ? $json->url : '',
        'field_item_type' => !empty($json->itemType) ? $json->itemType : '',
        'field_title' => !empty($json->title) ? $json->title : '',
        'field_body' => !empty($json->body) ? $json->body : '',
        'field_thumbnail_url' => !empty($json->thumbnailUrl) ? $json->thumbnailUrl : '',
        'field_author_name' => !empty($json->authorName) ? $json->authorName : '',
        'field_author_url' => !empty($json->authorUrl) ? $json->authorUrl : '',
        'field_response_json' => json_encode($json),
        // TODO - save in proper format
        //'field_published_at'  => !empty($json->publishedAt)? $json->publishedAt : '',
      ];
    }
    $storage = \Drupal::entityTypeManager()->getStorage('media');
    return $storage->create($values);
  }

  protected function fetchThumbnail(MediaInterface $entity) {
    if (empty($entity->field_thumbnail_url->uri)) {
      return $entity;
    }

    $client = new Client(['base_uri' => $entity->field_thumbnail_url->uri]);
    $request = $client->request("GET");

    // TODO: make try / catch
    if ($request->getStatusCode() == 200) {
      $image_data = $request->getBody()->getContents();
      $file_type = explode('/', $request->getHeader('Content-Type')[0]);
      $file_type = $file_type[1];

      $default_scheme = \Drupal::config('system.file')->get('default_scheme');
      $directory = $default_scheme . '://betterembed';
      \Drupal::service('file_system')
        ->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
      $random = new Random();
      $file = \Drupal::service('file.repository')->writeData($image_data,
        $directory . '/image_' . $random->word(5) . '.' . $file_type,
        FileSystemInterface::EXISTS_RENAME);
      $file->save();

      $entity->thumbnail->target_id = (int) $file->id();
      $entity->field_betterembed_media_image = $file;
    }
    else {
      $error = $request->getBody()->getContents();
      \Drupal::logger('BetterEmbed')
        ->notice("Embed ID @id: Couldn't get Thumbnail from @url: @error",
          [
            '@id'    => $this->id(),
            '@url'   => $this->field_thumbnail_url->uri,
            '@error' => $error
          ]);
    }
    return $entity;
  }

}
