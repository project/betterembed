<?php

namespace Drupal\betterembed\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;

/**
 * Class GlobalConfigForm.
 */
class GlobalConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Constructs a new GlobalConfigForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory)
  {
    parent::__construct($config_factory);
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'betterembed.globalconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'betterembed_global_config_form';
  }

  /**
   * {@inheritdoc}
   */
  static public function getDefaultConfig() {
    return [
      'skin' => 'none',
      'show_media' => FALSE,
//      'local_mode' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('betterembed.globalconfig');

    // Todo - minor - maybe this is not needed, have a look how other modules do this.
    //$default = self::getDefaultConfig();
    //$config = $config + $default;

    $form['skin'] = [
      '#type' => 'select',
      '#title' => $this->t('Skin'),
      '#description' => $this->t('The styling which will be used for Embeds.'),
      '#default_value' => $config->get('betterembed.skin'),
      '#options' => [
        'none' => $this->t("No Styling"),
        'classic' => $this->t("Classic"),
        'fancy' => $this->t("Fancy")
      ]
    ];

    $form['show_media'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show Media'),
      '#description' => $this->t('Chose how BetterEmbed should load Media.'),
      '#default_value' => $config->get('betterembed.show_media'),
      '#options' => [
        'none' => $this->t('None'),
        'remote' => $this->t('Load from remote location'),
        'local' => $this->t('Load locally'),
      ],
    ];

    $form['server_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Embed server URL'),
      '#description' => $this->t('Configure the location of the better embed server.'),
      '#default_value' => "https://api.betterembed.com",
    ];

    $form['custom_image_link_behaviour'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom preview image link behaviour'),
      '#description' => $this->t('Preview image by default links to the source URL. You can override it behaviour for specific source, by listing URL here. Second parameter would be custom class, so you can easily add CSS hover icon for example. Overridden preview image will load remote source in iframe on click.'),
      '#attributes' => [
        'placeholder' => $this->t('youtube.com | play'),
      ],
      '#default_value' => $config->get('betterembed.custom_image_link_behaviour'),
    ];

/*    $form['local_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Local Mode'),
      '#description' => $this->t('Embed Requests, if successfull, will be saved and loaded locally.'),
      '#default_value' => $config->get('betterembed.local_mode'),
    ];*/

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('betterembed.globalconfig');
    $config->set('betterembed.skin', $form_state->getValue('skin'));
    $config->set('betterembed.show_media', $form_state->getValue('show_media'));
    $config->set('betterembed.server_url', $form_state->getValue('server_url'));
    $config->set('betterembed.custom_image_link_behaviour', $form_state->getValue('custom_image_link_behaviour'));
//    $config->set('betterembed.local_mode', $form_state->getValue('local_mode'));
    $config->save();
  }

}
