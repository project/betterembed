<?php

namespace Drupal\betterembed\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\media\Entity\Media;

/**
 * Defines a confirmation form to fetch again betterembed entity.
 */
class RefreshBetterEmbedConfirmForm extends ConfirmFormBase {

  protected $entity_to_refresh;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Media $media = NULL) {
    $this->entity_to_refresh = $media;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, Media $media = NULL) {
    $betterembed_manager= \Drupal::service('betterembed');
    $betterembed_manager->refreshBetterEmbed($this->entity_to_refresh->id());

    $this->messenger()->addMessage(
      $this->t('BetterEmbed entity "%label" has been refreshed.', [
          '%label' => $this->entity_to_refresh->label(),
        ]
      ), 'status');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will override the current content of this entity. Any customizations will be lost.');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "confirm_fetch_again";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_content');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Do you really want to refresh this entity? "%label"', ['%label' => $this->entity_to_refresh->label()]);
  }

}
