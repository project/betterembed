<?php

namespace Drupal\betterembed\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\HtmlResponse;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Symfony\Component\HttpFoundation\Request;

class BetterEmbedController extends ControllerBase {

  public function render(Media $media, Request $request) {
    return new HtmlResponse($media->field_embed_html->value);
  }
}
